# README #

Welcome to the YCRepo!

This repository contains files and addons to install the YCRepo and the YCBeast Build for Kodi. Simply follow the Installation Guide.

### What is the YCBeast build? ###

Simply put, the YCBeast build is a version of Kodi/XBMC that comes pre-baked with all the most popular add-ons, plug-ins and repositories already downloaded – so you don’t have to install, tweak or configure anything.

I have tailor-made the YCBeast build for your Fire Stick. The YCBeast build gives you everything you’d want from a Kodi installation – and it’s quick and responsive, too..

### How do I get set up? ###

**Install the YCRepo and YCBeast Wizard**

* Open Kodi
* Select SYSTEM and/or YCBeast > File Manager
* Select Add Source
* Select None
* Type the following EXACTLY http://ycrepo.info/ycbeast/repo/ and select Done
* Highlight the box underneath Enter a name for this media Source, type ..YCRepo & select OK
* Go back to your Home Screen
* Select SYSTEM
* Select Add-Ons
* Select Install from zip file
* Select ..YCRepo
* Select repository.ycbeastrepository.x.x.x.zip
* Wait for Add-on enabled notification
* Select Install from repository
* Select YCRepo repository
* Select Program add-ons
* Select YCBeast Wizard
* Select Install
* Wait for Addon enabled notification
* YCBeast Wizard is now installed and ready to use.

Now to install the YCBeast Build we will use the YCBeast Wizard to Wipe/Clean Kodi and then install the YCbeast. Follow the steps below to complete.

**Install the YCBeast Build** 

* Select Add-Ons
* Select YCBeast Wizard
* Select Open
* Select Fresh Start This will wipe/clean Kodi and return it to it's initial state. Once complete reboot / pull the power cable from the FireStick, wait 5 seconds and then plug the power cable back in.
* Open Kodi
* Select SYSTEM and/or YCBeast
* Select Add-Ons
* Select YCBeast Wizard
* Select Open
* Select Install This will install the YCBeast Build for Kodi. Once complete reboot / pull the power cable from the FireStick, wait 5 seconds and then plug the power cable back in.

The YCBeast Build is now installed and ready to use. Start Kodi and begin enjoying the YCBeast Build.

### Where can I go for help? ###

Contact me via Twitter: [http://twitter.com/forthejest](Link URL)

* YouTube Channel: [http://bit.do/ycbeastyt](Link URL)
* Installation Guide (TEXT): [http://bit.do/ycbeastig](Link URL)
* Installation Guide (VIDEO): [http://bit.do/ycbeastvg](Link URL)
* YCBeast Wiki: [http://bit.do/ycbeastw](Link URL)
* Latest Newsletter: [http://bit.do/ycbeastn](Link URL)